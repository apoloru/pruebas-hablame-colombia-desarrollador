<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'modelos';

    /**
     * Run the migrations.
     * @table modelos
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idModelo');
            $table->integer('idMarca');
            $table->integer('idLinea');
            $table->string('nombreMarca', 50)->nullable()->default(null);
            $table->integer('estadoMarca')->nullable()->default(null);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->dateTime('update_at')->nullable()->default(null);

            $table->index(["idLinea"], 'idLinea');

            $table->foreign('idMarca', 'idMarca')
                ->references('idMarca')->on('marcas')
                ->onDelete('restrict')
                ->onUpdate('restrict');
                
            $table->index(["idMarca"], 'idMarca');

            $table->foreign('idLinea', 'idLinea')
                ->references('idLinea')->on('lineas')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
