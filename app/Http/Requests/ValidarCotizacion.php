<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidarCotizacion extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombres' => 'required',
            'apellidos' => 'required',
            'celular' => 'required|numeric|min:1',
            'genero' => 'required|numeric',
            'email' => 'required|email',
            'idMarca' => 'required|numeric|min:1',
            'idLinea' => 'required|numeric|min:1',
            'idModelo' => 'required|numeric|min:1',
            'piensaEstrenar' => 'required_if:estrenar,1',
            'categoria' => 'required_if:estrenar,1',
            'financiacionInteres' => 'required_if:estrenar,1',
        ];
    }
    public function messages() {
        return [
            "piensaEstrenar.required_if" => "Seleccionar tiempo si piensas estrenar.",
            "categoria.required_if" => "Seleccionar la categoria.",
            "financiacionInteres.required_if" => "Seleccionar la financiacion interes."
        ];
    }
}
