<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cotizaciones extends Model
{
    protected $table    = 'cotizaciones';
    protected $fillable = [
    	"idCotizacion",
    	"nombres",
    	"apellidos",
    	"genero",
    	"email",
    	"idMarca",
    	"idLinea",
    	"idModelo",
    	"tiempoEstrenar",
    	"financiacionInteres",
    	"politicas"
    ];
    public $timestamps = false;
}
