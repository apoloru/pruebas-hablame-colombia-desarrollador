<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lineas extends Model
{
    protected $table    = 'lineas';
    protected $fillable = [
    	"idLinea",
    	"idMarca",
    	"nombreMarca",
    	"estadoMarca"
    ];
}
