<!DOCTYPE html>
<html lang="es">
    <head>
        <title>
            Polo Rubiano
        </title>
        <!-- Meta-Tags -->
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="utf-8"/>
        <meta content="Prueba de desarrollo." name="keywords"/>
        <script>
            addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
        </script>
        <!-- //Meta-Tags -->
        <!-- css files -->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"/>

        <link href="{{ url('css/font-awesome.min.css') }}" media="all" rel="stylesheet" type="text/css"/>
        <link href="{{ url('css/style.css') }}" media="all" rel="stylesheet" type="text/css"/>
        <link href="{{ url('css/smoke.css') }}" rel="stylesheet"/>
        <link href="{{ url('css/smoke.min.css') }}" rel="stylesheet"/>
        <!-- //css files -->
        <link crossorigin="anonymous" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" rel="stylesheet"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.css" rel="stylesheet" type="text/css"/>
        <!-- google fonts -->
        <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"/>
        <!-- //google fonts -->
    </head>
    <body>
        <div class="signupform">
            <div class="container">
                <!-- main content -->
                <div class="agile_info">
                    <div class="w3l_form">
                        <div class="left_grid_info">
                            <h1>
                                Completa este formulario
                            </h1>
                            <p>
                                y entérate por qué somos una marca superior.
                            </p>
                            <div class="img-option" id="img-selection">
                            </div>
                            <div class="carousel">
                                <div class="carousel slide" data-ride="carousel" id="carouselExampleControls">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img alt="First slide" class="d-block w-100" src="http://assets.stickpng.com/thumbs/580b585b2edbce24c47b2d37.png"/>
                                        </div>
                                        <div class="carousel-item">
                                            <img alt="Second slide" class="d-block w-100" src="http://www.stickpng.com/assets/images/580b585b2edbce24c47b2bfc.png"/>
                                        </div>
                                        <div class="carousel-item">
                                            <img alt="Third slide" class="d-block w-100" src="http://www.mundoauto.net/wp-content/uploads/2013/12/comprar-coche-en-alcobendas-6.png"/>
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" data-slide="prev" href="#carouselExampleControls" role="button">
                                        <span aria-hidden="true" class="carousel-control-prev-icon">
                                        </span>
                                        <span class="sr-only">
                                            Anterior
                                        </span>
                                    </a>
                                    <a class="carousel-control-next" data-slide="next" href="#carouselExampleControls" role="button">
                                        <span aria-hidden="true" class="carousel-control-next-icon">
                                        </span>
                                        <span class="sr-only">
                                            Siguiente
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w3_info">
                        {!! Form::open(["role"=>"form", "id"=>"frInfo",'data-smk-icon'=>'fa-times']) !!}
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <div class="form-group">
                                    <input class="form-control" id="nombres" name="nombres" placeholder="Nombres" minlength="4" maxlength="50" required="true" type="text" onKeyPress="return allowLetters(event)" />
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="form-group">
                                    <input class="form-control" id="apellidos" name="apellidos" placeholder="Apellidos" required="true" type="text" minlength="4" maxlength="50"  onKeyPress="return allowLetters(event)" />
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="form-group">
                                    <input class="form-control" id="celular" name="celular" placeholder="Número de celular" required="true" type="number" min="1" maxlength="10" onKeyPress="return allowNumber(event)"/>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="form-group">
                                    <span>
                                        Género
                                    </span>
                                    <label class="radio-inline">
                                        <input id="genero1" name="genero" required="" type="radio" value="1"/>
                                        M
                                    </label>
                                    <label class="radio-inline">
                                        <input id="genero2" name="genero" required="" type="radio" value="2"/>
                                        F
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        @
                                    </span>
                                </div>
                                <input class="form-control" id="email" name="email" placeholder="Correo electrónico" required="" type="email"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <span>
                                    <strong>
                                        Vehículo actual
                                    </strong>
                                </span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="input-group">
                                    {!! Form::select('idMarca', $marcas,null,['id'=>'idMarca','class'=>'form-control input-lg idMarca','required'=>true]) !!}
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="input-group">
                                    {!! Form::select('idLinea', $lineas,null,['id'=>'idLinea','class'=>'form-control input-lg idLinea','required'=>true]) !!}
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="input-group">
                                    {!! Form::select('idModelo', $modelos,null,['id'=>'idModelo','class'=>'form-control input-lg idModelo','required'=>true]) !!}
                                </div>
                            </div>
                            <div class="col-md-10 mb-3">
                                <span style="font-size: 0.8em;">
                                    Tiene intención de cambiar su vehículo o adquirir uno nuevo?
                                </span>
                            </div>
                            <div class="col-md-2 mb-3">
                                <select id="estrenar" name="estrenar">
                                    <option value="0">
                                        No
                                    </option>
                                    <option value="1">
                                        Si
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="d-none" id="piensaEstrenar">
                            <div class="row">
                                <div class="col-md-12 mb-3" style="background-color: #939292;">
                                    <span>
                                        <strong>
                                            ¿En cuanto tiempo piensas estrenar?
                                        </strong>
                                    </span>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <div class="form-group">
                                        <label class="radio-inline">
                                            <input name="piensaEstrenar" required="" type="radio" value="1"/>
                                            1-3 meses
                                        </label>
                                        <label class="radio-inline">
                                            <input name="piensaEstrenar" required="" type="radio" value="2"/>
                                            3-6 meses
                                        </label>
                                        <label class="radio-inline">
                                            <input name="piensaEstrenar" required="" type="radio" value="3"/>
                                            más de 6
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2 mb-3">
                                    <span style="font-size: 0.7em;">
                                        ¿En qué vehículo está interesado?
                                    </span>
                                </div>
                                <div class="col-md-10 mb-3" id="categoria">
                                    <div class="form-group">
                                        <div class="cc-selector">
                                            <input id="bus" name="categoria" required="" type="radio" value="1"/>
                                            <label class="drinkcard-cc bus" for="bus">
                                            </label>
                                            <input id="auto" name="categoria" required="" type="radio" value="2"/>
                                            <label class="drinkcard-cc auto" for="auto">
                                            </label>
                                            <input id="camion" name="categoria" required="" type="radio" value="3"/>
                                            <label class="drinkcard-cc camion" for="camion">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-3" style="background-color: #939292;">
                                    <span>
                                        <strong>
                                            ¿Requiere financiación sin intereses?
                                        </strong>
                                    </span>
                                </div>
                                <div class="col-md-12 mb-3" id="financiacionInteres">
                                    <div class="form-group">
                                        <label class="radio-inline">
                                            <input id="financiacion1" name="financiacionInteres" required="" type="radio" value="1"/>
                                            12 meses
                                        </label>
                                        <label class="radio-inline">
                                            <input id="financiacion2" name="financiacionInteres" required="" type="radio" value="1"/>
                                            36 meses
                                        </label>
                                        <label class="radio-inline">
                                            <input id="financiacion3" name="financiacionInteres" required="" type="radio" value="1"/>
                                            60 meses
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            Autorizo a Spen Trucks a que me envie información comercial vía E-mail, teléfono, mensajes de texto, correo. Conozca más sobre nuestra Politíca de Privacidad, haciendo clic aquí.
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-info btn-block" id="guardarCotizacion" type="button">
                                Enviar
                            </button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- //main content -->
            </div>
            <!-- footer -->
            <div class="footer">
                <p>
                    © 2019 realizado por
                    <a href="mailto:apolorubiano@gmail.com?Subject=Hola%20...." target="_top">
                        apolorubiano@gmail.com
                    </a>
                </p>
            </div>
            <!-- footer -->
        </div>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
        </script>
        <script crossorigin="anonymous" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js">
        </script>
        <script crossorigin="anonymous" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.min.js" type="text/javascript">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.js">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js">
        </script>
        <script src="{{ url('js/smoke.js') }}">
        </script>
        <script src="{{ url('js/smoke.min.js') }}">
        </script>
        <script src="{{ url('js/blockUI.js') }}">
        </script>
        <script src="{{ asset('js/page.js') }}" type="text/javascript">
        </script>
        <script>
            $(function(){
                $("#idMarca").change(function(){
                    var valor = $(this).val();
                    token = '{{ csrf_token() }}';
                    if(valor != ""){
                        $.ajax({
                            url: "{{ route('idLinea') }}",
                            type: 'POST',
                            data: {idMarca: valor, _token: token},
                            success: function(response){
                                var data = response.lineas;
                                console.log(data);
                                $('#idLinea').empty();
                                $.each(data, function(key, element) {
                                    $('#idLinea').append("<option value='" + key + "'>" + element + "</option>");
                                });

                                $('#idModelo').empty();
                                $('#idModelo').append("<option value=''></option>");
                            }
                        });
                    }else{
                        $('#idLinea').empty();
                        $('#idLinea').append("<option value=''></option>");

                        $('#idModelo').empty();
                        $('#idModelo').append("<option value=''></option>");
                    }
                });

                $("#idLinea").change(function(){
                    var valor = $(this).val();
                    var idMarca = $("#idMarca").val();
                    token = '{{ csrf_token() }}';
                    if(valor != "" && idMarca != ""){
                        $.ajax({
                            url: "{{ route('idModelo') }}",
                            type: 'POST',
                            data: {idMarca: idMarca, idLinea: valor, _token: token},
                            success: function(response){
                                var data = response.modelos;
                                $('#idModelo').empty();
                                $.each(data, function(key, element) {
                                    $('#idModelo').append("<option value='" + key + "'>" + element + "</option>");
                                });
                            }
                        });
                    }else{
                        $('#idModelo').empty();
                        $('#idModelo').append("<option value=''></option>");
                    }
                });

                $("#guardarCotizacion").click(function(e) {
                    $.ajax({
                        url: "{{ route('guardarCotizacion') }}",
                        type: 'POST',
                        data: $("#frInfo").serialize(),
                        beforeSend: function(){
                            $.blockUI({ 
                                message: '<img src="http://parrelloautomotores.com/wp-content/plugins/cars-seller-auto-classifieds-script/images/loading-1.gif">', 
                                css: { 
                                    border: "0",
                                    background: "transparent",
                                    width: "200px"
                                },
                                overlayCSS:  { 
                                    backgroundColor: "#fff",
                                    opacity:         0.6, 
                                    cursor:          "wait" 
                                }
                            });
                        },
                        success: function(response){
                            console.log(response);
                            $.unblockUI();
                            swal("Bien!", "Se registro la solicitud y se realizo el envio de la cotización al E-mail ingresado!", "success")
                        },
                        error:function(response)
                        { 
                            $("input").css({"border": "1px solid #ccc"});
                            $("select").css({"border": "1px solid #ccc"});
                            $.unblockUI();

                            e.preventDefault();
                            if ($('#frInfo').smkValidate()) {
                                validate = true;
                                $.each(response.responseJSON['errors'], function(index, val){
                                    validate = false;
                                    document.getElementById(index).style.border = 'solid red';
                                });

                                if(validate == true){
                                    $.smkAlert({
                                        text: 'Se actualizo correctamente!',
                                        type: 'success'
                                    });
                                }
                            }
                            swal("¡Advertencia!", "Datos obligatorios resaltados en rojo, verificar si el dato solicitado es el correspondiente.", "warning");
                        }
                    })
                })

            })
        </script>
    </body>
</html>
