$(".img-option").hide();
$(function(){
    $("#camion").on("click", function(){
        $(".img-option").show();
        $('#img-selection').html('<img id="theImg" class="img-thumbnail" src="http://assets.stickpng.com/thumbs/580b585b2edbce24c47b2d37.png" />');
        $(".carousel").hide();
    })

    $("#bus").on("click", function(){
        $(".img-option").show();
        $('#img-selection').html('<img id="theImg" class="img-thumbnail"  src="http://www.stickpng.com/assets/images/580b585b2edbce24c47b2bfc.png" />');
        $(".carousel").hide();
    })

    $("#auto").on("click", function(){
        $(".img-option").show();
        $('#img-selection').html('<img id="theImg" class="img-thumbnail" src="http://www.mundoauto.net/wp-content/uploads/2013/12/comprar-coche-en-alcobendas-6.png" />');
        $(".carousel").hide();
    })

    $('.idMarca').select2();
    $('.idLinea').select2();
    $('.idModelo').select2();

    $('#estrenar').change(function(){
        valor = $(this).val();
        if(valor == 1)
        {
          $("#piensaEstrenar").removeClass("d-none");
        }else{
          $("#piensaEstrenar").addClass("d-none");
        }
    })

})

function allowNumber(e){
            
            tecla = (document.all) ? e.keyCode : e.which;

            //Tecla de retroceso para borrar, siempre la permite
            if (tecla==8){
                return true;
            }
                
            // Patron de entrada, en este caso solo acepta numeros
            patron =/[0-9]/;
            tecla_final = String.fromCharCode(tecla);
            return patron.test(tecla_final);
        }

        function allowLetter(e){
            key = e.keyCode || e.which;
            tecla = String.fromCharCode(key).toLowerCase();
            letras = "áéíóúabcdefghijklmnñopqrstuvwxyz";
            especiales = "8-37-39-46";

            tecla_especial = false
            for(var i in especiales){
                if(key == especiales[i]){
                    tecla_especial = true;
                    break;
                }
            }
            if(letras.indexOf(tecla)==-1 && !tecla_especial){
                return false;
            }
        }

        function allowLetters(e){
            key = e.keyCode || e.which;
            tecla = String.fromCharCode(key).toLowerCase();
            letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
            especiales = "8-37-39-46";

            tecla_especial = false
            for(var i in especiales){
                if(key == especiales[i]){
                    tecla_especial = true;
                    break;
                }
            }
            if(letras.indexOf(tecla)==-1 && !tecla_especial){
                return false;
            }
        }