<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCotizacionesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'cotizaciones';

    /**
     * Run the migrations.
     * @table cotizaciones
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idCotizacion');
            $table->string('nombres', 50)->nullable()->default(null);
            $table->string('apellidos', 50)->nullable()->default(null);
            $table->integer('genero');
            $table->string('email', 60)->nullable()->default(null);
            $table->integer('idMarca');
            $table->integer('idLinea');
            $table->integer('idModelo');
            $table->integer('tiempoEstrenar')->nullable()->default(null);
            $table->integer('financiacionInteres')->nullable()->default(null);
            $table->tinyInteger('politicas')->nullable()->default(null);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->dateTime('update_at')->nullable()->default(null);

            $table->index(["idLinea"], 'idLinea');

            $table->index(["idModelo"], 'idModelo');

            $table->index(["idMarca"], 'idMarca');


            $table->foreign('idMarca', 'idMarca')
                ->references('idMarca')->on('marcas')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('idLinea', 'idLinea')
                ->references('idLinea')->on('lineas')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('idModelo', 'idModelo')
                ->references('idModelo')->on('modelos')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
