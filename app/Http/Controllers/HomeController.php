<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Marcas;
use App\Models\Lineas;
use App\Models\Modelos;
use App\Models\Cotizaciones;
use Mail;

class HomeController extends Controller
{
    function index(Request $request)
    {
    	$marcas = ["" => "Seleccionar marca"] + Marcas::where("estadoMarca", 1)->orderBy('nombreMarca', 'ASC')->pluck("nombreMarca", "idMarca")->toArray();
    	$lineas = [];
    	$modelos = [];

    	return view('index', compact('marcas','lineas','modelos'));
    }

    function idLinea(Request $request)
    {
    	$lineas =
    		Lineas::
    		where("estadoMarca", 1)
    		->where("idMarca",$request->idMarca)
    		->orderBy('nombreMarca', 'ASC')
    		->pluck("nombreMarca", "idLinea")
    		->toArray();

    	return response()->json(["lineas" => $lineas]);
    }

    function idModelo(Request $request)
    {
    	$modelos = 
    		Modelos::
    		where("estadoMarca", 1)
    		->where("idMarca",$request->idMarca)
    		->where("idLinea",$request->idLinea)
    		->orderBy('nombreMarca', 'ASC')
    		->pluck("nombreMarca", "idLinea")
    		->toArray();

    	return response()->json(["modelos" => $modelos]);
    }

    public function guardarCotizacion(Request $request, Requests\ValidarCotizacion $validate)
    {
    	Cotizaciones::create($request->all());
    	$cotizacion = $request->all();
    	$this->sendEmail($cotizacion);

        return response()->json(["success" => true]);
    }

    private function sendEmail($cotizacion)
    {
        Mail::send('emails.cotizacion', [
            'cotizacion' => $cotizacion,
        ], function ($message) use ($cotizacion) {
            $message->to($cotizacion['email']);
            $message->subject("Hola ".$cotizacion['nombres'].", ...");
        });
    }
}
